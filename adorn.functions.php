<?php
    $adornFunctions = Array(
        'for (.+?) in (.+?)'    =>  'foreach($2 as $1):',
        'endfor'                =>  'endforeach;',
        'if (.+?)'              =>  'if($1):',
        'elseif (.+?)'          =>  'elseif($1):',
        'else'                  =>  'else:',
        'endif'                 =>  'endif;'
    );