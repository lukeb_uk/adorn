<?php
    class Adorn {
        protected $file;
        protected $values;
        protected $tableValues;
        protected $template;
        protected $view;
        protected $output;
        protected $adornFunctions;
        
        public function __construct(){
            include('adorn.functions.php');
            $this->adornFunctions = $adornFunctions;
        }
        
        public function setTemplate($file){
            $this->template = $file;
        }
        
        public function setView($name, $file){
            $this->view[$name] = $file;
        }
        
        public function setValue($key, $value){
            $this->values[$key] = $value;
        }
        
        public function buildPage(){
            
            // Does the template exist yo?
            if(file_exists($this->template)){
                
                // Load template
                $this->output = file_get_contents($this->template);
                
                $this->markupToPhp();
                
                return $this->output;
            }
        }
        
        private function markupToPhp(){
            
            preg_match_all('/{% view (.+?) %}/', $this->output, $viewMatches);
                
            foreach($viewMatches[1] as $thisView){
                
                if(isset($this->view[$thisView]) && !empty($this->view[$thisView]) && file_exists($this->view[$thisView])){
            
                    // Load sub template
                    $view = file_get_contents($this->view[$thisView]);
                    
                    $this->output = str_replace("{% view ".$thisView." %}", $view, $this->output);
                    
                    $this->functionLexer();
                    
                    $this->variableLexer();
                    
                    $this->executeLexedCode();
                }
            }
            
        }

        private function functionLexer(){
            foreach($this->adornFunctions as $pattern=>$replacement){
                $this->output = preg_replace("/{% ".$pattern." %}/", "<?php ".$replacement." ?>", $this->output);                
            }
        }
        
        private function variableLexer(){
            $this->output = preg_replace("/\{\{([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff\.]*?)\}\}/", '<?php echo {$1}; ?>', $this->output);
            $this->output = preg_replace_callback("/\{([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff\.]*?)\}/", array($this, "changeMultipartVar"), $this->output);
        }
        
        private function changeMultipartVar($value){
            $value = explode(".", $value[1]);
            
            $newValue = "\$this->values['".$value[0]."']";
            
            
            for($i=1; $i<count($value); $i++){
                $newValue .= "['".$value[$i]."']";
            }
            
            return $newValue;
        }
        
        private function turn_array($m){
            for ($z = 0;$z < count($m);$z++)
            {
                for ($x = 0;$x < count($m[$z]);$x++)
                {
                    $rt[$x][$z] = $m[$z][$x];
                }
            }   
           
            return $rt;
        }
        
        private function executeLexedCode(){
            ob_start();
            $tmp=array_search('uri', @array_flip(stream_get_meta_data($GLOBALS[mt_rand()]=tmpfile())));
            file_put_contents($tmp, $this->output);
            include($tmp);
            $this->output = ob_get_contents();
            ob_end_clean();
        }
    }
